// pour cleaner le champ à chaque refresh
document.getElementById('built-request-input').value = "";

const setsReferences = [
	{
		name : "Contentful - Content Delivery API",
		baseURL : "https://cdn.contentful.com",
		references : [
			{
				type : 'GET',
				name : "Space",
				definition : "Get a space",
				path : '/spaces/{space_id}?access_token={access_token}',
				parameters : [
					{
						type : 'string',
						name : '{space_id}',
						definition : "Alphanumeric id of the space to retrieve.",
						required : true,
						example : "yadj1kx9rmg0"
					},
					{
						type : 'string',
						name : '{access_token}',
						definition : "A production Content Delivery API key.",
						required : true,
						example : "fdb4e7a3102747a02ea69ebac5e282b9e44d28fb340f778a4f5e788625a61abe"
					}
				]
			},
			{
				type : 'GET',
				name : "Content model",
				definition : "Get the content model of a space",
				path : '/spaces/{space_id}/environments/{environment_id}/content_types?access_token={access_token}',
				parameters : [
					{
						type : 'string',
						name : '{space_id}',
						definition : "ID of the space in form of a string",
						required : true,
						example : "yadj1kx9rmg0"
					},
					{
						type : 'string',
						name : '{environment_id}',
						definition : "ID of the environment in form of a string",
						required : true,
						example : "staging"
					},
					{
						type : 'string',
						name : '{access_token}',
						definition : "A production Content Delivery API key.",
						required : true,
						example : "fdb4e7a3102747a02ea69ebac5e282b9e44d28fb340f778a4f5e788625a61abe"
					}
				]
			},
			{
				type : 'GET',
				name : "Content type",
				definition : "Get a single content type",
				path : '/spaces/{space_id}/environments/{environment_id}/content_types/{content_type_id}?access_token={access_token}',
				parameters : [
					{
						type : 'string',
						name : '{space_id}',
						definition : "ID of the space in form of a string",
						required : true,
						example : "yadj1kx9rmg0"
					},
					{
						type : 'string',
						name : '{environment_id}',
						definition : "ID of the environment in form of a string",
						required : true,
						example : "staging"
					},
					{
						type : 'string',
						name : '{content_type_id}',
						definition : "ID of the content type in form of a string",
						required : true,
						example : "2PqfXUJwE8qSYKuM0U6w8M"
					},
					{
						type : 'string',
						name : '{access_token}',
						definition : "A production Content Delivery API key.",
						required : true,
						example : "fdb4e7a3102747a02ea69ebac5e282b9e44d28fb340f778a4f5e788625a61abe"
					}
				]
			},
			{
				type : 'GET',
				name : "Entries collection",
				definition : "Get all entries of a Space",
				path : '/spaces/{space_id}/environments/{environment_id}/entries?access_token={access_token}',
				parameters : [
					{
						type : 'string',
						name : '{space_id}',
						definition : "Alphanumeric id of the space to retrieve.",
						required : true,
						example : "yadj1kx9rmg0"
					},
					{
						type : 'string',
						name : '{environment_id}',
						definition : "ID of the environment in form of a string",
						required : true,
						example : "staging"
					},
					{
						type : 'string',
						name : '{access_token}',
						definition : "A production Content Delivery API key.",
						required : true,
						example : "fdb4e7a3102747a02ea69ebac5e282b9e44d28fb340f778a4f5e788625a61abe"
					}
				]
			},
			{
				type : 'GET',
				name : "Entry",
				definition : "Get a single entry",
				path : '/spaces/{space_id}/environments/{environment_id}/entries/{entry_id}?access_token={access_token}',
				parameters : [
					{
						type : 'string',
						name : '{space_id}',
						definition : "Alphanumeric id of the space to retrieve.",
						required : true,
						example : "yadj1kx9rmg0"
					},
					{
						type : 'string',
						name : '{environment_id}',
						definition : "ID of the environment in form of a string",
						required : true,
						example : "staging"
					},
					{
						type : 'string',
						name : '{entry_id}',
						definition : "Alphanumeric id of the entry to retrieve",
						required : true,
						lookup : {
							useReference : 'Entries collection',
							itemsPropertyName : 'items',
							sysPropertyName : 'sys',
							idPropertyName : 'id'
						},
						example : "5KsDBWseXY6QegucYAoacS"
					},
					{
						type : 'string',
						name : '{access_token}',
						definition : "A production Content Delivery API key.",
						required : true,
						example : "fdb4e7a3102747a02ea69ebac5e282b9e44d28fb340f778a4f5e788625a61abe"
					}
				]
			}
		],
		memorizableParameters : [
			'{space_id}',
			'{access_token}',
			'{environment_id}'
		]
	}
]

var rememberedValues = [];

// fonction pour lister les sets
var setsListElement = document.getElementById('sets-list');

function addSetToList(set) {
	var node = document.createElement('li');
	node.classList.add('box', 'set-box');
	node.setAttribute('set-name', set.name);
	node.addEventListener('click', selectSet);
	node.innerHTML = `<p><span class="has-text-weight-bold">${set.name}</span>`;
	setsListElement.appendChild(node);
}

function createSetsList() {
	for (i=0 ; i < setsReferences.length ; i++) {
		addSetToList(setsReferences[i])
	}
}
createSetsList();

// fonction pour sélectionner un set
function selectSet() {
	var setBoxes = document.getElementsByClassName('set-box');
	for (i = 0; i < setBoxes.length; i++) {
		setBoxes[i].classList.remove('has-background-grey-lighter');
	}

	var setName = this.getAttribute('set-name');
	this.classList.add('has-background-grey-lighter');
	createReferencesList(setName)
}

// fonctions pour lister les references d'un set et ses paramètres globaux
var referencesListElement = document.getElementById('references-list');

function addReferenceToList(setName, reference) {
	var node = document.createElement('li');
	node.classList.add('box', 'reference-box');
	node.setAttribute('set-name', setName);
	node.setAttribute('reference-name', reference.name);
	node.addEventListener('click', selectReference);
	node.innerHTML = 
	`<p><span class="has-text-weight-bold">${reference.name}</span> <span class="tag is-link is-light is-normal">${reference.type}</span></p>
	<p>${reference.definition}</p>`;
	referencesListElement.appendChild(node);
}

var globalParametersListElement = document.getElementById('global-parameters-list');

function addGlobalParameterToList(parameterName) {
	var label = document.createElement('label');
	label.classList.add('label');
	label.innerHTML = `${parameterName}`;
	globalParametersListElement.appendChild(label);
	var input = document.createElement('input');
	input.setAttribute('id', 'global-input-'+parameterName)
	input.classList.add('input');
	input.setAttribute('type', "text");
	input.setAttribute('parameter-name', parameterName);
	globalParametersListElement.appendChild(input);
}

function createReferencesList(setName) {
	document.getElementById('reference-empty-state-message').classList.add('is-hidden');

	const setIndex = setsReferences.findIndex(element => element.name === setName);
	var setReferences = setsReferences[setIndex].references
	var setGlobalParameters = setsReferences[setIndex].memorizableParameters

	referencesListElement.innerHTML = "";
	for (i=0 ; i < setReferences.length ; i++) {
		addReferenceToList(setName, setReferences[i])
	}

	globalParametersListElement.innerHTML = "";
	for (i=0 ; i < setGlobalParameters.length ; i++) {
		addGlobalParameterToList(setGlobalParameters[i]);
	}

	// dans la modale des paramètres globaux : ajouter le bouton pour sauvegarder les valeurs
	function displayButtonSave() {
		var cta = document.createElement('button');
		cta.classList.add('button', 'cta', 'is-fullwidth', 'is-link');
		cta.setAttribute('id', 'save-global-parameters');
		cta.setAttribute('set-name', setName);
		cta.addEventListener('click', selectSetToSaveGlobalParameters);
		cta.innerHTML = "Save";
		globalParametersListElement.appendChild(cta);
	}
	displayButtonSave();
}

// fonctions pour afficher/masquer la modale listant tous les paramètres globaux d'un set
function showModalGlobalParameters() {
	document.getElementById('set-global-parameters').classList.add('is-active')
}

function hideModalGlobalParameters() {
	document.getElementById('set-global-parameters').classList.remove('is-active')
}

document.getElementById('toggle-global-parameters').addEventListener('click', showModalGlobalParameters)
document.getElementById('set-global-parameters_modal-background').addEventListener('click', hideModalGlobalParameters)

// fonction pour sélectionner la référence pour laquelle générer la request
function selectSetToSaveGlobalParameters() {
	var setName = this.getAttribute('set-name');
	saveValuesForGlobalParameters(setName)
	hideModalGlobalParameters();
}

// fonction sauvegarder la valeur d'un paramètre global
function saveValuesForGlobalParameters(setName) {
	const setIndex = setsReferences.findIndex(element => element.name === setName);
	var setMemorizableParameters = setsReferences[setIndex].memorizableParameters; 

	// fonction pour ajouter une valeur en mémoire
	function saveValue(parameterName, value) {
		var obj = {setName: setName, parameter : parameterName, value : value};
		rememberedValues.push(obj);
	}

	// fonction pour update une valeur déjà en mémoire
	function updateValue(index, value) {
		rememberedValues[index].value = value;
	}

	for (i=0 ; i < setMemorizableParameters.length ; i++) {
		var inputValue = document.getElementById('global-input-' + setMemorizableParameters[i]).value;
		var indexOfRememberedValue = rememberedValues.findIndex(element => element.setName === setName && element.parameter === setMemorizableParameters[i]);
		if(indexOfRememberedValue >= 0) {
			updateValue(indexOfRememberedValue, inputValue);
		} else {
			saveValue(setMemorizableParameters[i], inputValue);
		}
	}
}

// fonction pour sélectionner une référence
function selectReference() {
	var referenceBoxes = document.getElementsByClassName('reference-box');
	for (i = 0; i < referenceBoxes.length; i++) {
		referenceBoxes[i].classList.remove('has-background-grey-lighter');
	}

	var setName = this.getAttribute('set-name');
	var referenceName = this.getAttribute('reference-name');
	this.classList.add('has-background-grey-lighter');
	displayRequestBuilder(setName, referenceName)
}

// fonction pour afficher les infos de la référence
var builderFormElement = document.getElementById('reference-form')

function displayReference(reference) {
	var node = document.createElement('div');
	node.setAttribute('id', 'form-'+reference);
	node.classList.add('box');
	node.setAttribute('reference-name', reference.name);
	node.innerHTML = 
	`<p><span class="has-text-weight-bold">${reference.name}</span> <span class="tag is-link is-light is-normal">${reference.type}</span></p>
	<p>${reference.definition}</p>
	<br/>
	`;
	builderFormElement.innerHTML = "";
	builderFormElement.appendChild(node);
}

// fonction pour construire le formulaire d'une référence
function buildReferenceForm(setName, reference) {
	// fonction pour ajouter au formulaire un input texte
	const formElement = document.getElementById('form-'+reference)
	function addInputText(parameter) {
		var label = document.createElement('label');
		label.classList.add('label');
		label.innerHTML = `${parameter.name}`;
		formElement.appendChild(label);
		var input = document.createElement('input');
		input.setAttribute('id', 'input-'+parameter.name)
		input.classList.add('input');
		input.setAttribute('type', "text");
		input.setAttribute('parameter-name', parameter.name);
		var rememberedValueIndex = rememberedValues.findIndex(element => element.setName === setName && element.parameter === parameter.name);
		if(rememberedValueIndex >= 0) {
			input.value = rememberedValues[rememberedValueIndex].value;
		};
		formElement.appendChild(input);
		if(parameter.hasOwnProperty('lookup')) {
			var link = document.createElement('a');
			link.setAttribute('set-name', setName);
			link.setAttribute('reference-name', reference.name);
			link.setAttribute('parameter-name', parameter.name);
			link.innerHTML = "Get list of all the items";
			link.addEventListener('click', selectParameter);
			formElement.appendChild(link)
		};
	}

	// créer les champs un à un
	const referenceParameters = reference.parameters;
	for (i=0 ; i < referenceParameters.length ; i++) {
		switch(referenceParameters[i].type) {
			case 'string' :
				addInputText(referenceParameters[i]);
				break;
		}
	}

	// ajouter le bouton pour générer la request
	function displayButtonGenerator() {
		var cta = document.createElement('button');
		cta.classList.add('button', 'cta', 'is-fullwidth', 'is-link');
		cta.setAttribute('id', 'request-generator-button');
		cta.setAttribute('set-name', setName);
		cta.setAttribute('reference-name', reference.name);
		cta.addEventListener('click', selectRequestReference);
		cta.innerHTML = "Build";
		formElement.appendChild(cta);
	}
	displayButtonGenerator();
}

// fonction pour afficher le builder de la request correspondant à la référence sélectionnée
function displayRequestBuilder(setName, referenceName) {
	document.getElementById('build-empty-state-message').classList.add('is-hidden');

	const setIndex = setsReferences.findIndex(element => element.name === setName);
	var setReferences = setsReferences[setIndex].references;

	const referenceIndex = setReferences.findIndex(element => element.name === referenceName);
	var reference = setReferences[referenceIndex];
	displayReference(reference);
	buildReferenceForm(setName, reference);

	document.getElementById('built-request-input').value = "";
}

// fonction pour sélectionner la référence pour laquelle générer la request
function selectRequestReference() {
	var setName = this.getAttribute('set-name');
	var referenceName = this.getAttribute('reference-name');
	document.getElementById('built-request-input').value = buildRequest(setName, referenceName);
}

// fonction pour construire une request
function buildRequest(setName, referenceName) {
	const setIndex = setsReferences.findIndex(element => element.name === setName);
	var setBaseURL = setsReferences[setIndex].baseURL;

	const referenceIndex = setsReferences[setIndex].references.findIndex(element => element.name === referenceName);
	var reference = setsReferences[setIndex].references[referenceIndex];
	var referencePath = reference.path;
	var referenceParameters = reference.parameters;

	var parametersNamesMappedWithValues = {};
	for (i=0 ; i < referenceParameters.length ; i++) {
		parametersNamesMappedWithValues[referenceParameters[i].name] = document.getElementById('input-' + referenceParameters[i].name).value;
	}
	var parametersNamesKeys = Object.keys(parametersNamesMappedWithValues)
	var re = new RegExp(parametersNamesKeys.join('|'),'gi')

	var fullPath = setBaseURL + (referencePath.replace(re, (matched) => {return parametersNamesMappedWithValues[matched]}))
	return fullPath;
}

var copyRequestBtn = document.getElementById('btn-copy-request');
copyRequestBtn.addEventListener('click', function(event) {
	var requestInput = document.getElementById('built-request-input');
	requestInput.select();
	document.execCommand('copy')

	copyRequestBtn.innerHTML = 'Copied !'
	setTimeout(() => {
		copyRequestBtn.innerHTML = 'Copy'
	}, 1000)
});

// fonction pour sélectionner un paramètre d'une référence dans un set, pour retrouver les valeurs possible de ce paramètre
function selectParameter() {
	var setName = this.getAttribute('set-name');
	var referenceName = this.getAttribute('reference-name')
	var parameterName = this.getAttribute('parameter-name');
	getItems(setName, referenceName, parameterName)
}

// fonction pour afficher une liste d'items existants (utile pour retrouver l'id d'un item en particulier et l'utiliser pour construire une autre request)
async function getItems(setName, referenceName, parameterName) {
    const setIndex = setsReferences.findIndex(element => element.name === setName);
    var setBaseURL = setsReferences[setIndex].baseURL;

    const referenceIndex = setsReferences[setIndex].references.findIndex(element => element.name === referenceName);
	var reference = setsReferences[setIndex].references[referenceIndex];

	const parameterIndex = reference.parameters.findIndex(element => element.name === parameterName);
	var referenceToUse = reference.parameters[parameterIndex].lookup.useReference;
	var itemsPropertyName = reference.parameters[parameterIndex].lookup.itemsPropertyName;
	var sysPropertyName = reference.parameters[parameterIndex].lookup.sysPropertyName;
	var idPropertyName = reference.parameters[parameterIndex].lookup.idPropertyName;

	const itemsListElement = document.getElementById('parameter-items-list')

	let url = buildRequest(setName, referenceToUse);
	try {
		let res = await fetch(url);
		let resJSON = await res.json();
		var resObjItemsReference = resJSON[itemsPropertyName];
		console.log(resObjItemsReference)
		for(i=0 ; i < resObjItemsReference.length ; i++) {
	        var item = document.createElement('li');
			item.classList.add('box');
			var itemId = resObjItemsReference[i][sysPropertyName][idPropertyName];
			item.setAttribute('parameter', reference.parameters[parameterIndex].name);
			item.setAttribute('item-id', itemId);
			item.innerHTML =
			`<p class="has-text-weight-bold">${itemId}</p>`;
			item.addEventListener('click', selectIdItem)
			itemsListElement.appendChild(item);
		}
		showModalParameterItems();
    } catch (error) {
        console.log(error);
    }
}

// fonction pour sélectionner l'id d'un item
function selectIdItem() {
	var parameterName = this.getAttribute('parameter');
	var id = this.getAttribute('item-id')
	getAndPasteIdItem(parameterName, id)
}

// fonction pour récupérer l'id un item et le coller dans l'input
function getAndPasteIdItem(parameterName, id) {
	document.getElementById('input-' + parameterName).value = id;
	hideModalParameterItems();
}

// fonctions pour afficher/masquer la modale listant les items d'un paramètre
function showModalParameterItems() {
	document.getElementById('parameter-items').classList.add('is-active')
}

function hideModalParameterItems() {
	document.getElementById('parameter-items').classList.remove('is-active')
}
document.getElementById('parameter-items_modal-background').addEventListener('click', hideModalParameterItems)
